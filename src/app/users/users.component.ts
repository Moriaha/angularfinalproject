import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { UsersService } from './users.service';
import {AngularFire , AuthProviders, AuthMethods, FirebaseListObservable, FirebaseApp } from 'angularfire2';
import { User } from '../user/user';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';





@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
   styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }  
  `]
})
export class UsersComponent implements OnInit {

  /*  users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ] 

  currentUser = this.users[1];
  */
user:User= {name: '', password: ''  ,email:'',  pic:''};
  users;
  isLoading=false;
  currentUser;
  editMode= false;
  usersLogIN={name:'', email:'', pic:'', provider:'', id:''};
  storageRef;  storageRef1;
 // name;email;pic;
 changeN=false;changeE=false;changeP=false;

onSubmit(form:NgForm){
  this.changeN=false;this.changeE=false;this.changeP=false;
  this.user={name: this.usersLogIN.name , password:'' ,email:this.usersLogIN.email,  pic: this.usersLogIN.pic};
  console.log(form)
  if((<HTMLInputElement>document.getElementById('name')).value !=  this.usersLogIN.name){
    this.user.name = (<HTMLInputElement>document.getElementById('name')).value
    this.changeN=true;
    this.af.auth.getAuth().auth.updateProfile({displayName: this.user.name , photoURL: this.usersLogIN.pic})
  }
  /*if((<HTMLInputElement>document.getElementById('email')).value !=  this.usersLogIN.email){
    this.user.email = (<HTMLInputElement>document.getElementById('email')).value
    this.changeE=true;
    this.af.auth.getAuth().auth.updateEmail(this.user.email);
  }*/
  if((<HTMLInputElement>document.getElementById('pic')).files.length > 0){
            for (let selectedFile of [(<HTMLInputElement>document.getElementById('pic')).files[0]]) {
            console.log(selectedFile);
            let path = `${selectedFile.name}`;
            this.user.pic=path;
            var iRef = this.storageRef1.child(path);
            iRef.put(selectedFile).then((snapshot) => {console.log('Uploaded a blob or file! Now storing the reference ')})    
            this.changeP=true;
            this.af.auth.getAuth().auth.updateProfile({displayName: this.usersLogIN.name , photoURL: this.user.pic})
    }
    this.upload()
  }
  if(this.changeP || this.changeN){
    console.log('call to update')
   this._userService.updateUser(this.user , this.user.email)}
   this.router.navigate(['/groups'])
   this.router.navigate(['/friends'])
   this.user = {name: '', password: ''  ,email:'',  pic:''}
   this.editMode=!this.editMode;

  }


saveChanges(){

  this.changeN=false;this.changeE=false;this.changeP=false;
  this.user={name: this.usersLogIN.name , password:'' ,email:this.usersLogIN.email,  pic: this.usersLogIN.pic};

  if((<HTMLInputElement>document.getElementById('name')).value !=  this.usersLogIN.name){
    this.user.name = (<HTMLInputElement>document.getElementById('name')).value
    this.changeN=true;
 //   this.af.auth.getAuth().auth.updateProfile({displayName: this.user.name , photoURL: this.usersLogIN.pic})
  }

  if((<HTMLInputElement>document.getElementById('pic')).files.length > 0){
            for (let selectedFile of [(<HTMLInputElement>document.getElementById('pic')).files[0]]) {
            console.log(selectedFile);
            let path = `${selectedFile.name}`;
            this.user.pic=path;
            var iRef = this.storageRef1.child(path);
            iRef.put(selectedFile).then((snapshot) => {console.log('Uploaded a blob or file! Now storing the reference ')})    
            this.changeP=true;
    //        this.af.auth.getAuth().auth.updateProfile({displayName: this.user.name , photoURL: this.user.pic})
    }

    
  }

  if(this.changeP || this.changeN){
    console.log('call to update')
  this.af.auth.getAuth().auth.updateProfile({displayName: this.user.name , photoURL: this.user.pic})
   this._userService.updateUser(this.user ,  this.usersLogIN.id)
  }
  //  this.router.navigate(['/friends'])
   this.changeN=false;this.changeE=false;this.changeP=false;
   this.user = {name: '', password: ''  ,email:'',  pic:''}
   this.editMode=!this.editMode;
   this.ngAfterViewChecked();
   
  this.appComp.ngAfterViewChecked();

}



    upload() {
    //    let success = false;
        for (let selectedFile of [(<HTMLInputElement>document.getElementById('pic')).files[0]]) {
            console.log(selectedFile);
            let path = `${selectedFile.name}`;
            var iRef = this.storageRef1.child(path);
            iRef.put(selectedFile).then((snapshot) => {console.log('Uploaded a blob or file! Now storing the reference ')})
            this.user.pic=path;
    }
  }


changeEditMode(){
this.editMode=!this.editMode;
}

constructor(public af:AngularFire , private _userService:UsersService, private appComp:AppComponent, @Inject(FirebaseApp) firebaseApp: any, private router:Router, private cdr: ChangeDetectorRef) { 
      this.storageRef1 = firebaseApp.storage().ref('image.png/');

      this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN.email = auth.google.email  
          this.usersLogIN.name=auth.google.displayName
          this.usersLogIN.pic=auth.google.photoURL
          this.usersLogIN.provider='3';
          if(this.usersLogIN.pic!= null){
         // this.storageRef = firebaseApp.storage().ref().child('image.png/'+this.usersLogIN.pic).getDownloadURL().then(url=>{this.isLoading=true})
        this.storageRef = this.usersLogIN.pic
        console.log(this.storageRef, 11111111111)
      this.isLoading=true
    console.log(this.storageRef,this.isLoading, 11111111111)}
          else {this.isLoading=true}
 //     this._userService.getUsreName(this.usersLogIN.email).subscribe(name=> this.usersLogIN.name=name)
      }
      else if(auth && auth.provider==4){
          this.usersLogIN.email = auth.auth.email
       //   this.usersLogIN.name = this._userService.getUsreName(this.usersLogIN.email)
          this.usersLogIN.name=auth.auth.displayName
          this.usersLogIN.pic=auth.auth.photoURL
          this.usersLogIN.provider='4';
         // firebaseApp.storage().ref('image.png/').child(this.usersLogIN.pic).getDownloadURL().then(downloadURL=>this.storageRef =downloadURL)
         if(this.usersLogIN.pic!= null){
          this.storageRef=firebaseApp.storage().ref('image.png/').child(this.usersLogIN.pic).getDownloadURL().then(url=> {this.storageRef=url,console.log(this.storageRef), this.isLoading=true})
        console.log(44444444444)}
          else {
            this.isLoading=true
            console.log(123123123123)}
        //    this._userService.getUsreName(this.usersLogIN.email).subscribe(name=> this.usersLogIN.name=name)

     }})

     this.usersLogIN.id=this._userService.getUsreId( this.usersLogIN.email) 

     
}

  ngOnInit() {

    this._userService.getUsers().subscribe(usersData =>{this.users=usersData; });

          this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN.name=auth.google.displayName
          this.isLoading=true
      }
      else if(auth && auth.provider==4){
          this.usersLogIN.name=auth.auth.displayName

     }})
     if(this.usersLogIN.pic!= null){}
          else {this.isLoading=true}

  }

      ngAfterViewChecked() {

      this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN.name=auth.google.displayName
      }
      else if(auth && auth.provider==4){
          this.usersLogIN.name=auth.auth.displayName

     }})
    this.cdr.detectChanges();
  }

}





       