import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';
import { PostsService } from '../posts/posts.service';
import {AngularFire , AuthProviders} from 'angularfire2';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { UsersService } from '../users/users.service';


@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css'],
})
export class PostFormComponent implements OnInit {

//content; 
//user;
//groupId;
//dateTime;
emailUser;
post={content:'', dateTime: '', group:'', user:''}

onSubmit(form:NgForm){
 // if((<HTMLInputElement>document.getElementById('content')) !=null ){
this.post.content=(<HTMLInputElement>document.getElementById('content')).value
this.post.dateTime= new Date().toString()
this.post.dateTime=this.post.dateTime.slice(0,this.post.dateTime.lastIndexOf(":"))
this._postsService.addPost(this.post);
(<HTMLInputElement>document.getElementById('content')).value=''
}


  constructor(public af:AngularFire, private _postsService:PostsService, private activatedRoute: ActivatedRoute,  private _userService:UsersService) { }

  ngOnInit() {
      this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.emailUser = auth.google.email    
     //   this.post.user=this._userService.getUsreId(this.emailUser);
      }
        else if(auth && auth.provider==4){
          this.emailUser = auth.auth.email
   //   this.post.user=this._userService.getUsreId(this.emailUser);
    }})
        

         let items = this.af.database.list('/users');
       items.subscribe(items => {items.forEach(item => (item.email == this.emailUser ? this.post.user=item.$key : ''))});


        this.activatedRoute.params.subscribe((params: Params) => {
        this.post.group = params['id'];
      });
  }

}
