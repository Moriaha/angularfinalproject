import { Component, OnInit , Output, EventEmitter, OnDestroy, ChangeDetectorRef} from '@angular/core';
import { GroupsService } from './groups.service';
import {AngularFire , AuthProviders} from 'angularfire2';
import { UsersService } from '../users/users.service';
import { User } from '../user/user';
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  @Output() onCreate = new EventEmitter<GroupsComponent>();
  users:User;
  groups;
  isLoading=false;
  currentUser;
  usersLogIN;
  thisAdnim=false;
  preName;
  newUser;
  nogroup=true;
  nogroupadmin=true;

  constructor(private activatedRoute: ActivatedRoute,public af:AngularFire, private _groupsService:GroupsService , private _userService:UsersService, private cdr: ChangeDetectorRef) { }
seeAll(){
this.thisAdnim=false
}
seeAdmin(){
this.thisAdnim=true
}
deleteMember(name , group){
  this._groupsService.deleteUser(name , group);
}
deleteGroup(group){
  this._groupsService.deleteGroup(group);
}
editGroup(group){
  this._groupsService.updateGroup(group);
}
addMember(user, group){
this.newUser=false;
this._groupsService.addUser(user, group);
}
noGroups(){
  this.nogroup=false;
}
noGroupsAdmin(){
  this.nogroupadmin=false;
}

  ngOnInit() {
        this._groupsService.getGroups().subscribe(groupsData =>{this.groups=groupsData; this.isLoading=false;console.log(this.groups); this.newUser=true});
        

        this.activatedRoute.params.subscribe((params: Params) => {
          if(params['id']==2){
        this.thisAdnim=true;}
        });


        this._userService.getUsers().subscribe(usersData =>{this.users=usersData});

          this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN = auth.google.email    
      }
      else if(auth && auth.provider==4){
          this.usersLogIN = auth.auth.email
      }})
    this.onCreate.emit();
  }

    ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

}
