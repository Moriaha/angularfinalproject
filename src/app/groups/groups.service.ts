import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire , AuthProviders} from 'angularfire2';

@Injectable()
export class GroupsService {

groupsObservable;
user = {};
usersLogIN;
group={};
groupMembers;
newUser=true;


getGroups(){
//this.groupsObservable = this.af.database.list('/groups');
//this.groupsObservable = this.groupsObservable.subscribe(items => {items.emailAd=[] , items.forEach(item => (item.admin == this.usersLogIN ? '' : '')) })
this.groupsObservable=this.af.database.list('/groups').map(
groups => 
  {groups.map( group=> {
      group.memberss=[];
      group.admins=[];
      for(var p in group.members){
        group.memberss.push(this.af.database.object('/users/' + p))
    }
      for(var p in group.admin){
        group.admins.push(this.af.database.object('/users/' + p))
    }
  });
    return groups;
  }
);
return this.groupsObservable;
}


updateGroup(group){
  let userKey = group.$key;
  let userData = {name: group.name};
  this.af.database.object('/groups/'+userKey).update(userData);
}

deleteUser(user, group){
  let groupKey = group.$key;
  let userKey = user.$ref.key;
  this.af.database.object('/groups/' + groupKey + '/members/' + userKey).remove();
}
deleteGroup(group){
  let groupKey = group.$key;
  this.af.database.object('/groups/' + groupKey).remove();
}

addGroup(group){
  console.log(this.group);
  let groupId = this.groupsObservable.push(group);
  this.af.database.object('/groups/' + groupId.path.o[1] + '/admin' ).remove();
  console.log( groupId.path.o[1])
  let userData = {[group.admin] : true};
  this.af.database.object('/groups/' + groupId.path.o[1] + '/admin/' ).update(userData);

}
getGroupName(id){
  //let name = this.af.database.object('/groups/'+id + '/name')
  //console.log(name, 111111111111)
 // return name
let name;
 let item = this.af.database.list('/groups');
 // item.subscribe(items => {items.forEach(item => (item.$key == id ? name=item.name : ''))});
//  console.log(item, 111111111111)
  return item
}

addUser(user, group){
  
  let groupKey = group.$key;
  let userData = {[user] : true};
  //this.groupMembers = this.af.database.list('/groups/'+groupKey + '/members/');
  //this.groupMembers.subscribe(items => {this.newUser=true, items.forEach(item => {(item.$key == user ? this.newUser=false : '')})})
return this.af.database.object('/groups/'+groupKey + '/members/').update(userData);
//console.log(this.newUser)

//  return this.newUser
}


  constructor(public af:AngularFire, private _http:Http, )
 {
    this.af.auth.subscribe(user => {
      if(user) {
        // user logged in
        this.user = user
        
      }
      else {
        // user not logged in
        this.user = {};
      }
    });


      this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN = auth.google.email    
      }
      else if(auth && auth.provider==4){
          this.usersLogIN = auth.auth.email
      }})
  }

}