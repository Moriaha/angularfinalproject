import { BrowserModule } from '@angular/platform-browser';
import { NgModule , ModuleWithProviders} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule, AuthMethods, AuthProviders} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostsComponent } from './posts/posts.component';
import { UserFormComponent } from './user-form/user-form.component';
import { GloginComponent } from './glogin/glogin.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupsService } from './groups/groups.service';
import { PostsService } from './posts/posts.service';
import { GroupComponent } from './group/group.component';
import { GroupFormComponent } from './group-form/group-form.component';
import { PostFormComponent } from './post-form/post-form.component';


  export const firebaseConfig = {
    apiKey: "AIzaSyDkSuWbr-ClZuIBMI8HHIyUZACMtEcUxjo",
    authDomain: "angular-final-project.firebaseapp.com",
    databaseURL: "https://angular-final-project.firebaseio.com",
    storageBucket: "angular-final-project.appspot.com",
    messagingSenderId: "410413899064"
  };



export const appRoutes: Routes =[
	  {path:'friends', component:UsersComponent},
    {path:'groups', component:GroupsComponent},
    {path:'groups/:id', component:GroupsComponent},
	  {path:'posts/:id', component:PostsComponent},
    {path:'posts', component:PostsComponent},
    {path:'login', component:GloginComponent},
    {path:'newGroup', component:GroupFormComponent},
	  {path:'', component:GloginComponent},
	  {path:'**', component:PageNotFoundComponent}
	];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostsComponent,
    UserFormComponent,
    GloginComponent,
    GroupsComponent,
    GroupComponent,
    GroupFormComponent,
    PostFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    RouterModule,
   RouterModule.forRoot(appRoutes),
   //AngularFireModule.initializeApp(firebaseConfig,{provider: AuthProviders.Google,method: AuthMethods.Popup}),
  AngularFireModule.initializeApp(firebaseConfig) 
  //  AngularFireModule.initializeApp(firebaseConfig,{provider: AuthProviders.Password,method: AuthMethods.Password})
  ],
  providers: [UsersService, GroupsService, PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
