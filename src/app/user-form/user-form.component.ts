import { Component, OnInit, Output, EventEmitter , Inject} from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../user/user';
import { UsersService } from '../users/users.service';
import {AngularFire , AuthProviders, AuthMethods, FirebaseListObservable, FirebaseApp } from 'angularfire2';



@Component({
  selector: 'jce-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})

export class UserFormComponent implements OnInit {
@Output() userAddedEvent = new EventEmitter<User>();
 user:User = {name:'', password:'', email:'', pic:''};

  users;
  isAuth = false;
  authColor = 'warn';
  isLog=true;
  name='';
  logSuccess=false;
  signSuccess=false;
  notValid=false;
  notValid1=false;
  pic;
  storageRef;desertRef;
 
onSubmit(form:NgForm){
    this.notValid=false;
    this.notValid1=false;
    this.signSuccess=false;
  console.log(form)
  this.name=this.user.name;
  if(this.isLog==true){
      this.af.auth.login({email: this.user.email, password: this.user.password},{provider: AuthProviders.Password,method: AuthMethods.Password,}).then((success) => {this.logSuccess=true; this.user = {name: '', password: ''  ,email:'',  pic:''}}).catch(()=>this.notValid=true);
    }
  
  else{
        console.log(this.user , 1)
      if(this.user.password.length >=6){
        if((<HTMLInputElement>document.getElementById('pic')).files.length > 0){
     //   this.user.pic = [(<HTMLInputElement>document.getElementById('pic')).files[0]][0].name;
    //    this.pic = [(<HTMLInputElement>document.getElementById('pic')).files[0]][0].name;
        this.upload()
      }
      
            this.af.auth.createUser({email: this.user.email, password: this.user.password}).then((user) => {  this.af.auth.getAuth().auth.sendEmailVerification() , this.signSuccess=true , this.userAddedEvent.emit(this.user), this.adduser(this.user), this.user = {name: '', password: ''  ,email:'',  pic:''}}).catch(()=> {this.notValid=true}); 
 
}
      else{
        this.notValid1=true;
      }
      //if(this.signSuccess){
          //    this.af.auth.login({email: this.user.email, password: this.user.password},{provider: AuthProviders.Password,method: AuthMethods.Password,});
                // 
      //    this.adduser(this.user);
    //  }else{
          
      }
  }


 //  let userProfile = this.user; userProfile.displayName = `${this.user.name}`;user.auth.updateProfile(userProfile)


    upload() {
        // Create a root reference
     //   let storageRef = firebase.storage().ref();
        
        let success = false;
        // This currently only grabs item 0, TODO refactor it to grab them all
        for (let selectedFile of [(<HTMLInputElement>document.getElementById('pic')).files[0]]) {
            console.log(selectedFile);
            // Make local copies of services because "this" will be clobbered
         //   let router = this.router;
         //   let af = this.af;
         //   let folder = this.folder;
            let path = `${selectedFile.name}`;
            var iRef = this.storageRef.child(path);
            iRef.put(selectedFile).then((snapshot) => {console.log('Uploaded a blob or file! Now storing the reference ')})
           // this.pic = this.storageRef.child('image.png/${selectedFile.name}').getDownloadURL()
            this.user.pic=path;
            this.pic=path;
            //this.af.database.list(`/${folder}/images/`).push({ path: path, filename: selectedFile.name })
      //      });
             
    }
  }

changelog(sign:string=''){
  if(sign=="in"){
    this.isLog=true
  }
  else {
    this.isLog=false}
}

adduser(user){
//user.auth.providerData[0].displayName=this.name;
this._userService.addUser(user);
}     

forgotPass(){
  this.af.auth.getAuth().auth.sendEmailVerification()
}

  constructor(private _userService:UsersService, public af:AngularFire, @Inject(FirebaseApp) firebaseApp: any) { 

    console.log(af);  
      this.af.auth.subscribe(user => {this._getUserInfo(user), console.log(this.user , 2)}, error => console.trace(error));
       this.storageRef = firebaseApp.storage().ref('image.png/');
       //this.desertRef = this.storageRef.child('image.png/cdc.jpg').getDownloadURL();
     //  console.log(this.desertRef)
    //   this.storageRef = firebaseApp.storage().ref().child('image.png');

      
  }

  ngOnInit() {

    this._userService.getUsers().subscribe(usersData =>{this.users=usersData; 
    console.log(this.users , 3)});

  }


 /* private _changeState(user: any = null) {
    if(user) {
      this.isAuth = true;
      this.authColor = 'primary';
      this.user = this._getUserInfo(user)
      console.log(this.user)
    //  this.user1={name: this._getUserInfo(user).displayName , email: this._getUserInfo(user).email };
     // this._userService.addUser(this.user1);
    }
    else {
      this.isAuth = false;
      this.authColor = 'warn';
      this.user = {name:'', password:'', email:''};
    }
  }*/

    private _getUserInfo(user: any): any {
    if(!user) {
      return {};
    }
  //  user.auth.providerData[0].displayName=this.name;
  if(this.name != ''){
    this.af.auth.getAuth().auth.updateProfile({displayName: this.name, photoURL: this.pic})}
    //this.af.auth.subscribe(auth=> auth.auth.updateProfile({displayName: this.name, photoURL: this.pic}))
    let data = user.auth.providerData[0];
    

  //  this.af.auth.subscribe(auth => {auth.auth.displayName=this.name}); 
    console.log(this.name)
    return {
      name: data.displayName,
      photo: data.photoURL,
      email: data.email,
      provider: data.providerId
    };
  }


}
