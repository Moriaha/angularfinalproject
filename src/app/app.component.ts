import { Component , OnInit, ChangeDetectorRef} from '@angular/core';
import {AngularFire , AuthProviders,AuthMethods} from 'angularfire2';
import { UsersService } from './users/users.service';
import { Router } from '@angular/router';
import { UserFormComponent } from './user-form/user-form.component';
import {RouterModule, Routes} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'Welcome to Live Chat';

  isAuth = false;
  authColor = 'warn';
  user = {};
  user1={}
id;
usersLogIN;

  login(from: string) {
    this.af.auth.login({provider: this._getProvider(from),method: AuthMethods.Popup}); 
  }

 
logout() {
  this.af.auth.logout();
  console.log(this.user);
}


  constructor(public af:AngularFire , private _userService:UsersService, private router:Router, private cdr: ChangeDetectorRef) {
    console.log(af);  
      this.af.auth.subscribe(user => this._changeState(user), error => console.trace(error));
      this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN = auth.google.displayName    
      }
      else if(auth && auth.provider==4){
          this.usersLogIN = auth.auth.displayName
      }})
  };

  private _changeState(user: any = null) {
    if(user) {
      this.isAuth = true;
      this.authColor = 'primary';
      this.user = this._getUserInfo(user)
      console.log(user.provider)
      if(user.provider != 4){
      this._userService.addUserG(this.user);}
    }
    else {
      this.isAuth = false;
      this.authColor = 'warn';
      this.user = {};
    }
  }

    private _getUserInfo(user: any): any {
    if(!user) {
      return {};
    }
    let data = user.auth.providerData[0];
    return {
      name: data.displayName,
      avatar: data.photoURL,
      email: data.email,
      provider: data.providerId
    };
  }

  private _getProvider(from: string) {
    switch(from){
      case 'twitter': return AuthProviders.Twitter;
      case 'facebook': return AuthProviders.Facebook;
      case 'github': return AuthProviders.Github;
      case 'google': return AuthProviders.Google;
    }
  }

    ngOnInit() {
      this.af.auth.subscribe(user => {
        if(user) {

                 if(user && user.provider==3) {
          this.usersLogIN = user.google.displayName    
      }
      else if(user && user.provider==4){
          this.usersLogIN = user.auth.displayName}
          //user log in
        //  this.router.navigate(['/users']).then(()=>console.log('success')).catch(()=> console.log('not success'))

        }
        else {
          this.router.navigate(['/login']).then(()=>console.log('success')).catch(()=> console.log('not success'))

        }});
    }

      ngAfterViewChecked() {

      this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN=auth.google.displayName
      }
      else if(auth && auth.provider==4){
          this.usersLogIN=auth.auth.displayName

     }})
    this.cdr.detectChanges();
  }
 


}
