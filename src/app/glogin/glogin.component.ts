import { Component, OnInit } from '@angular/core';
import {AngularFire , AuthProviders,AuthMethods} from 'angularfire2';
import { UsersService } from '../users/users.service';
import { Router } from '@angular/router';
import { UserFormComponent } from '../user-form/user-form.component';
import {RouterModule, Routes} from '@angular/router';


@Component({
  selector: 'jce-glogin',
  templateUrl: './glogin.component.html',
  styleUrls: ['./glogin.component.css']
})
export class GloginComponent implements OnInit {

  isAuth = false;
  authColor = 'warn';
  user = {};
  user1={};
id;

  login(from: string) {
    this.af.auth.login({provider: this._getProvider(from),method: AuthMethods.Popup}); 
  }

 
logout() {
  this.af.auth.logout();
  console.log(this.user);

}


  constructor(public af:AngularFire , private _userService:UsersService, private router:Router) {
    console.log(af);  
      this.af.auth.subscribe(user => this._changeState(user), error => console.trace(error));
  };

  private _changeState(user: any = null) {
    if(user) {
      this.isAuth = true;
      this.authColor = 'primary';
      this.user = this._getUserInfo(user)
      console.log(user.provider)
      if(user.provider != 4){
      this._userService.addUserG(this.user);}
    }
    else {
      this.isAuth = false;
      this.authColor = 'warn';
      this.user = {};
    }
  }

    private _getUserInfo(user: any): any {
    if(!user) {
      return {};
    }
    let data = user.auth.providerData[0];
    return {
      name: data.displayName,
      avatar: data.photoURL,
      email: data.email,
      provider: data.providerId
    };
  }

  private _getProvider(from: string) {
    switch(from){
      case 'twitter': return AuthProviders.Twitter;
      case 'facebook': return AuthProviders.Facebook;
      case 'github': return AuthProviders.Github;
      case 'google': return AuthProviders.Google;
    }
  }

    ngOnInit() {
      this.af.auth.subscribe(user => {
        if(user) {
          //user log in
          this.router.navigate(['/groups']).then(()=>console.log('success')).catch(()=> console.log('not success'))
        }
        else {
          this.router.navigate(['/login']).then(()=>console.log('success')).catch(()=> console.log('not success'))
        }});





    }



}
