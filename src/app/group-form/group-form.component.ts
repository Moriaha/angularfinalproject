import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';
import { GroupsService } from '../groups/groups.service';
import { UsersService } from '../users/users.service';
import { User } from '../user/user';
import{Group} from '../group/group'
import {AngularFire , AuthProviders} from 'angularfire2';
import { Router } from '@angular/router';



@Component({
  selector: 'app-group-form',
  templateUrl: './group-form.component.html',
  styleUrls: ['./group-form.component.css']
})
export class GroupFormComponent implements OnInit {



group:Group={name:'',members:[], admin:''};
groups;
fillName = true;
users:User;
usersLogIN;

onSubmit(form:NgForm){

//validation
    if (  this.group.name == ''){
      this.fillName=false;
    }
    else{
      this.fillName=true;
    }
  if(this.group.name != ''){
    this.group.admin = this._userService.getUsreId(this.usersLogIN);
    this.group.members[this.group.admin]=true;
    this._groupsService.addGroup(this.group);
    console.log(this.group);
    this.group={name:'',members:[], admin:''};
    this.router.navigate(['/groups'])
  }
}

addMember(e , member){
  console.log(e.target.checked , member)
  if(e.target.checked){
    this.group.members[member]=true;
    console.log(this.group.members)
  }
  else{
    //this.group.members.splice(this.group.members.indexOf(member), 1);
    delete this.group.members[member]
    console.log(this.group.members)
  }
}

  constructor(public af:AngularFire, private _groupsService:GroupsService , private _userService:UsersService, private router:Router) { }

  ngOnInit() {
    this._groupsService.getGroups().subscribe(postsData => {this.groups=postsData});
    this._userService.getUsers().subscribe(usersData =>{this.users=usersData});

    this.af.auth.subscribe(auth => {
        if(auth && auth.provider==3) {
          this.usersLogIN = auth.google.email
          this.usersLogIN=this._userService.getUsreId(this.usersLogIN);    
      }
      else if(auth && auth.provider==4){
          this.usersLogIN = auth.auth.email
          this.usersLogIN=this._userService.getUsreId(this.usersLogIN);    
      }})

  }

}