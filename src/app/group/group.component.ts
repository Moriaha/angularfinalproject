import { Component, OnInit , Output, EventEmitter } from '@angular/core';
import { GroupsService } from '../groups/groups.service';
import { UsersService } from '../users/users.service';
import { User } from '../user/user';
import{Group} from './group'

@Component({
  selector: 'jce-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css'],
  inputs:['group' , 'thisAdnim']
})
export class GroupComponent implements OnInit {
@Output() deleteEvent = new EventEmitter<Group>();
@Output() editEvent = new EventEmitter<Group>();

group:Group;
users:User;
isEdit=false;
name1;

sendDelete() { 
 this.deleteEvent.emit(this.group);
}
editMode() { 
  this.isEdit=true;
  this.name1=this.group.name;
}
saveChange() { 
  this.isEdit=false;
  if(this.group.name != this.name1){
  this.editEvent.emit(this.group);
console.log('edit')
}
}
cancelEdit() { 
  this.isEdit=false;
  this.group.name=this.name1;
}

constructor( private _userService:UsersService) {
 }

  ngOnInit() {
    this._userService.getUsers().subscribe(usersData =>{this.users=usersData; console.log(this.users)});
  }

}
