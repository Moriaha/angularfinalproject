import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import {Http} from '@angular/http';


@Injectable()
export class PostsService {

postsObservable;

getPosts(){
this.postsObservable=this.af.database.list('/posts').map(
posts => 
  {posts.map( post=> {
    post.userName=[];
    for(var p in post.user){
      post.userName.push(this.af.database.object('/users/' + p))
    }});
    return posts;
  }
);
return this.postsObservable;
}

addPost(post){
console.log( post)
  let postId = this.postsObservable.push(post);
  this.af.database.object('/posts/' + postId.path.o[1] + '/user' ).remove();
  let userData = {[post.user] : true};
  this.af.database.object('/posts/' + postId.path.o[1] + '/user/' ).update(userData);

}

  constructor(private af:AngularFire) { }

}
