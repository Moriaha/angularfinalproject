import { Component, OnInit , OnDestroy, Inject, ElementRef, AfterViewChecked, ViewChild} from '@angular/core';
import { PostsService } from './posts.service';
import { GroupsService } from '../groups/groups.service';
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
    styles: [`
    .posts li {cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active,
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]
})
export class PostsComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;


  posts;
groupId;
groupName;
currentPost;

select(post){
this.currentPost=post;
}

date(post){
  if (post.dateTime ){

  }

}

  constructor(private _postsService:PostsService , private activatedRoute: ActivatedRoute, private _groupsService:GroupsService) { 
  }

  ngOnInit() {
        this.scrollToBottom();

      this._postsService.getPosts().subscribe(postsData =>{this.posts=postsData; console.log(this.posts)});
       this.activatedRoute.params.subscribe((params: Params) => {
        this.groupId = params['id'];
        console.log(this.groupId);
      });
 //     this.groupName = this._groupsService.getGroupName(this.groupId)
      this._groupsService.getGroupName(this.groupId).subscribe(postsData =>{this.groupName=postsData; console.log(this.groupName)})
      }

    ngAfterViewChecked() {        
        this.scrollToBottom();        
    } 

    scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }                 
    }


}
